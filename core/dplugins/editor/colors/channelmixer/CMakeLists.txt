#
# Copyright (c) 2015-2022 by Gilles Caulier, <caulier dot gilles at gmail dot com>
#
# Redistribution and use is allowed according to the terms of the BSD license.
# For details see the accompanying COPYING-CMAKE-SCRIPTS file.

APPLY_COMMON_POLICIES()

include(MacroDPlugins)

include_directories($<TARGET_PROPERTY:Qt${QT_VERSION_MAJOR}::Gui,INTERFACE_INCLUDE_DIRECTORIES>
                    $<TARGET_PROPERTY:Qt${QT_VERSION_MAJOR}::Widgets,INTERFACE_INCLUDE_DIRECTORIES>
                    $<TARGET_PROPERTY:Qt${QT_VERSION_MAJOR}::Core,INTERFACE_INCLUDE_DIRECTORIES>

                    $<TARGET_PROPERTY:KF5::ConfigCore,INTERFACE_INCLUDE_DIRECTORIES>
                    $<TARGET_PROPERTY:KF5::I18n,INTERFACE_INCLUDE_DIRECTORIES>
)

set(channelmixertoolplugin_SRCS
    ${CMAKE_CURRENT_SOURCE_DIR}/channelmixertoolplugin.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/channelmixertool.cpp
)

DIGIKAM_ADD_EDITOR_PLUGIN(NAME    ChannelMixerTool
                          SOURCES ${channelmixertoolplugin_SRCS}
)
